import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login me",
      home: Scaffold(
        appBar: AppBar(title: Text('Login')),
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String password;
  late String lastname;
  late String name;
  late String birthyear;
  late String streetAddress;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: formKey,
        child: Column(
          children: [
            fieldEmailAddress(),
            Container(margin: EdgeInsets.only(top: 40.0),),
            fieldLastName(),
            Container(margin: EdgeInsets.only(top: 40.0),),
            fieldName(),
            Container(margin: EdgeInsets.only(top: 40.0),),
            fieldbirthYear(),
            Container(margin: EdgeInsets.only(top: 40.0),),
            fieldAddress(),
            Container(margin: EdgeInsets.only(top: 40.0),),
            loginButton()
          ],
        ),
      )

    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: 'Email address'
      ),
      validator: (value) {
        if (!value!.contains('@') || value!.isEmpty) {
          return 'Please input valid Email.';
        }
        return null;
      },
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldLastName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Last Name'
      ),
      validator: (value) {
        if (value!.length < 30 || value!.isEmpty) {
          return 'Please input valid Last Name.';
        }
        return null;
      },
      onSaved: (value) {
        lastname = value as String;
      },
    );
  }

  Widget fieldName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Name'
      ),
      validator: (value) {
        if (value!.length < 30 || value!.isEmpty) {
          return 'Please input valid Name.';
        }
        return null;
      },
      onSaved: (value) {
        name = value as String;
      },
    );
  }

  Widget fieldbirthYear() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Birth Year'
      ),
      validator: (value) {
        if (value!.isEmpty||value!.length < 4) {
          return 'Please enter Birth Year';
        }
          return null;
      },
      onSaved: (value) {
        birthyear = value as String;
      },
    );
  }

  Widget fieldAddress() {
    return TextFormField(
      keyboardType: TextInputType.streetAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Address'
      ),
      validator: (value) {
        if (value!.length < 30 ||value!.isEmpty) {
          return 'Please input valid Street Address.';
        }
        return null;
      },
      onSaved: (value) {
        streetAddress = value as String;
      },
    );
  }





  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress, Demo only: $password');
          }
        },
        child: Text('Login')
    );
  }
}